package HomeWork6;
import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private Species species;
    private String nickname = null;
    private int age;
    private int trickLevel;
    private String[] habits;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);

    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age);
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public int getAge() {
        return age;
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    @Override
    public String toString() {
        return this.species.name() + species +
                " {nickname= " + "'" + nickname + "'" +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits);
    }


    public String eat() {
        return "I am eating";
    }

    public String respond() {
        return "Hello, owner. I am ".concat(getNickname()).concat(". I miss you!");
    }

    public String foul() {

        return "I need to cover it up";
    }


}
