package HomeWork6;
public enum Species {
    DOG(false, 4, true),
    CAT(false, 4, true),
    RABBIT(false, 4, true),
    FISH(false, 0,true),
    BIRD(true, 2, true),
    HORSE(false, 4, true);


    private final boolean flyOrNotFly;
    private final int howLegsHaveGot;
    private final boolean hasFur;
    Species species;


    Species(boolean flyOrNotFly, int howLegsHaveGot, boolean hasFur) {
        this.flyOrNotFly = flyOrNotFly;
        this.howLegsHaveGot = howLegsHaveGot;
        this.hasFur = hasFur;
    }


    @Override
    public String toString() {
        return String.format("{" +
                "fly=" + flyOrNotFly +
                ", howLegsHaveGot=" + howLegsHaveGot +
                ", canSwim=" + hasFur +
                '}');
    }
}
